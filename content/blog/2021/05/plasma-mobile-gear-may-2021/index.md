---
title: "Plasma Mobile Gear May 2021"
subtitle: Monthly release for Plasma Mobile applications
SPDX-License-Identifier: CC-BY-4.0
date: 2021-05-10
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
---

Plasma Mobile team is happy to announce PlaMo Gear release service.

This releases are aimed towards providing monthly release for smaller applications which make important part of Plasma Mobile experience.

You can find details about sources in [information page](/info/plasma-mobile-gear-21-05/)
