---
title: Plasma Dialer 24.08 is out
date: 2024-08-14T18:00:00Z
---

After a long wait, Plasma Dialer 24.08 is finally out. This released is based
on Qt6 and contains 17 months of bug fixing as well as small
improvements all other the place.

![Screenshot of the dialer page](dialer.png)

![Screenshot of the empty call history](call-history.png)

## Packager Section

You can find the package on
[download.kde.org](https://download.kde.org/stable/plasma-dialer/plasma-dialer-24.08.0.tar.xz.mirrorlist)
and it has been signed with my [Carl's GPG key](https://carlschwan.eu/gpg-02325448204e452a/).