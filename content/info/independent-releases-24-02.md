---
version: "24.02"
title: "Independent Releases 24.02"
type: info
date: 2024-02-28
---

Plasma Mobile Gear is now discontinued, with applications making their way to the main KDE releases.

However, some applications have not yet been moved, and so they have had independent releases:

- plasma-settings
- plasma-phonebook
- spacebar

We intend to also release plasma-dialer as soon as possible, but there is a [critical issue](https://invent.kde.org/plasma-mobile/plasma-dialer/-/issues/74) that is holding us back from doing so.
